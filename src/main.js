import Vue from 'vue'
import App from './App.vue'
import VuePluralize from 'vue-pluralize'

Vue.use(VuePluralize)

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
