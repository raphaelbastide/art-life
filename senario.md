
## TODO
- game dynamic: unlock actions
  - how do you unlock them
- think: how long does the game should last ?

- think of a way to store specific tick interval (like money is decreased every two ticks)
- design a full workshop experience with creating artworks
- add random events (like "oopsie workshop burned down u lost everything" )
- maybe add color when ressources are updated ?
- design interface
- order list of achievement in order of appearance (⚠️prise de tête)
- when the number change, the divs shift a bit: fix that
- time / score redondant ? maybe time shouldn't be displayed
- improve score


## DONE
- ~~switch teaching clock to global tick~~ (cancelled)
- fix resting box
- fixed sell (automaticly) if popular but commented it
- focus linked to global tick
- chose not to link teaching to global tick, but added check for paused game
- score value = total of all other gains (if you make 20 money and 10 popularity from action, score = 30)
- global score: implement + chose values
- make a profile: level of privilege + introvert / extravert
- add impact to personalityTypes ie if introvert lose energy when people
- fix grammar in achievement sentences (1 singular = 2 plural)
- changed "publish" to "create"
- move play/pause button elsewhere => make a design for paused game + make it so the game is pause when the tab is not active
- added dev model
- add condition do unlock destroy



## Senario
### Phases de jeu potentielles:
 1. Problèmes d'inspiration
 2. Problèmes d'argent
 3. Problèmes d'énergie
 4. ? ( Vieillesse plus de popularité, se renouvelle pas )

### 0bjectifs
- vendre un œuvre
- faire une expo (collective / personnelle)
- devenir enseignant ?
- avoir un·e galeriste


### Comment gagner de l'argent ?
- demander une bourse / résidence -> la bourse peut être donnée ou non
- vendre une œuvre

### Comment gagner de la popularité ?
- post sur réseaux
- network
- documentation du travail
- en enseignant / organisant des workshops ?
- followers on social media ?


### Comment gagner de l'énergie ?
- repos
- reconnaissance des pairs
- (si extravert) voir des gens


### Comment ganger de l'inspiration ?
- repos
- voir une expo ?
- read a book ?


### Comment créer des œuvres ?
- unlock différents medias ?
- via le texte ?
- à quel point ça doit être facile ?


### Catastrophes
- frais innatendus genre cotisation
- rencontrer une personne toxique
