<!-- ## Phase I -->

Each step must be executed in order to access the next one
1. Create (at least) 2 artworks
2. Share artworks is unlocked. Sharing = showing the world
  - If you share bad artwork, unlock possibility of destroying artworks
3. Go see exhibition is unlocked.  See 2 exhibitions.
4. Selling artworks is now unlocked.


 *End of phase I ?*

<!-- ## Phase II -->

5. Create (at least) 6 artworks
6. Go networking (make at least one contact)
7. Document your work
8. Make a portfolio
9. Apply for residency
10. Get granted residency
11. Become a teacher
12. ~~die~~
