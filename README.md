# Artist Survival Game

Inspiration, Energy, Money, Time…

## Install
```
npm install
```

## Start
```
npm start
```

By Raphaël Bastide, assisted by [Alice Ricci](https://alicericci.eu/)
